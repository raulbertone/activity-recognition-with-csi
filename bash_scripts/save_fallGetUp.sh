#!/bin/bash 

    maxCycles=$3
    declare  counter=0
    ext='.dat';
    
    #pid_= 0
    while [ $maxCycles -gt $counter ]
    do

echo "
 ██████╗ ███████╗████████╗    ██████╗ ███████╗ █████╗ ██████╗ ██╗   ██╗
██╔════╝ ██╔════╝╚══██╔══╝    ██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝
██║  ███╗█████╗     ██║       ██████╔╝█████╗  ███████║██║  ██║ ╚████╔╝ 
██║   ██║██╔══╝     ██║       ██╔══██╗██╔══╝  ██╔══██║██║  ██║  ╚██╔╝  
╚██████╔╝███████╗   ██║       ██║  ██║███████╗██║  ██║██████╔╝   ██║   
 ╚═════╝ ╚══════╝   ╚═╝       ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝   
"
	
    sleep 5

     echo "
███████╗ █████╗ ██╗     ██╗     
██╔════╝██╔══██╗██║     ██║     
█████╗  ███████║██║     ██║     
██╔══╝  ██╔══██║██║     ██║     
██║     ██║  ██║███████╗███████╗
╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝
"
     filename="falling_$1_$2_$counter$ext"
 
     sudo  /home/csi/Atheros-CSI-Tool-UserSpace-APP/recvCSI/recv_csi  /home/csi/raulCSI/data/$filename > /dev/null 2>&1 &

     pid_=$!  
     echo "SAVE AS $filename PID: $pid_" 
     sleep 5
     sudo kill -15 $!
    
     filename="getUp_$1_$2_$counter$ext"
     sudo  /home/csi/Atheros-CSI-Tool-UserSpace-APP/recvCSI/recv_csi  /home/csi/raulCSI/data/$filename > /dev/null 2>&1 &
     

echo "
 ██████╗ ███████╗████████╗    ██╗   ██╗██████╗     
██╔════╝ ██╔════╝╚══██╔══╝    ██║   ██║██╔══██╗    
██║  ███╗█████╗     ██║       ██║   ██║██████╔╝    
██║   ██║██╔══╝     ██║       ██║   ██║██╔═══╝     
╚██████╔╝███████╗   ██║       ╚██████╔╝██║         
 ╚═════╝ ╚══════╝   ╚═╝        ╚═════╝ ╚═╝   
"
sleep 15
((counter++))

    done

echo "
██████╗  ██████╗ ███╗   ██╗███████╗
██╔══██╗██╔═══██╗████╗  ██║██╔════╝
██║  ██║██║   ██║██╔██╗ ██║█████╗  
██║  ██║██║   ██║██║╚██╗██║██╔══╝  
██████╔╝╚██████╔╝██║ ╚████║███████╗
╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚══════╝
"
 



#!/bin/bash 
echo "
██╗    ██╗ █████╗ ██╗     ██╗  ██╗
██║    ██║██╔══██╗██║     ██║ ██╔╝
██║ █╗ ██║███████║██║     █████╔╝ 
██║███╗██║██╔══██║██║     ██╔═██╗ 
╚███╔███╔╝██║  ██║███████╗██║  ██╗
 ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
"
	
    sleep 10
    maxCycles=$3
    declare  counter=0
    ext='.dat';
    
    #pid_= 0
    while [ $maxCycles -gt $counter ]
    do
     echo "Recording  ..... "
     filename="walking_$1_$2_$counter$ext"
 
     sudo  /home/csi/Atheros-CSI-Tool-UserSpace-APP/recvCSI/recv_csi  /home/csi/raulCSI/data/$filename > /dev/null 2>&1 &

     pid_=$!  
     echo "SAVE AS $filename PID: $pid_" 
     sleep 10
     sudo kill -15 $!
    ((counter++))

     
    done

echo "
██████╗  ██████╗ ███╗   ██╗███████╗
██╔══██╗██╔═══██╗████╗  ██║██╔════╝
██║  ██║██║   ██║██╔██╗ ██║█████╗  
██║  ██║██║   ██║██║╚██╗██║██╔══╝  
██████╔╝╚██████╔╝██║ ╚████║███████╗
╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚══════╝
"
 



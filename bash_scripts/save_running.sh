#!/bin/bash 
echo "
██████╗ ██╗   ██╗███╗   ██╗
██╔══██╗██║   ██║████╗  ██║
██████╔╝██║   ██║██╔██╗ ██║
██╔══██╗██║   ██║██║╚██╗██║
██║  ██║╚██████╔╝██║ ╚████║
╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
"
	
    sleep 10
    maxCycles=$3
    declare  counter=0
    ext='.dat';
    
    #pid_= 0
    while [ $maxCycles -gt $counter ]
    do
     echo "Recording  ..... "
      filename="running_$1_$2_$counter$ext"
      #if [ $pid_ -gt 0 ]
      #then 
	echo "killing $!" #$pid_"
     # sudo kill -15 $! #$pid_ 
      #fi
 
     sudo  /home/csi/Atheros-CSI-Tool-UserSpace-APP/recvCSI/recv_csi  /home/csi/raulCSI/$filename > /dev/null 2>&1 &
     echo "background proc: $!";     
     pid_=$!  
     echo "SAVE AS $filename PID: $pid_" 
     sleep 10
     sudo kill -15 $! #$pid_ 
    ((counter++))

    done

echo "
██████╗  ██████╗ ███╗   ██╗███████╗
██╔══██╗██╔═══██╗████╗  ██║██╔════╝
██║  ██║██║   ██║██╔██╗ ██║█████╗  
██║  ██║██║   ██║██║╚██╗██║██╔══╝  
██████╔╝╚██████╔╝██║ ╚████║███████╗
╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚══════╝
"
 



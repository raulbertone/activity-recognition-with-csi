# Activity Recognition with CSI 

Abstract—Human activity recognition based on channel state
information (CSI) obtained from consumer Wi-Fi devices has
proven to be a viable alternative to sensor based approaches. 
This software uses the Atheros-CSI-Tool to reproduce the results 
obtained on other platforms.

We attempted to distinguish 8 activities performed by a subject
in an indoor environment. One of these activities was a ”fall”, a
particularly significant use case when monitoring elderly or not
self-sufficient people. The gathered data was subjected to minimal
pre-processing (sanitisation from bad network packets and
denoising with wavelets), and then analysed using a LSTM neural network. 
The results were disappointing: an acceptable accuracy was only achieved when the number of
classes was lowered to 4 (or less). Noisy data was, in our opinion,
the main cause.

For more information please see the Final_Report PDF in this same repository.

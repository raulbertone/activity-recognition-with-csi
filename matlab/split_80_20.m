%% =====================================================================================
%% Splits the data 80/20
%%
%%      Author:  Raul Bertone, Elis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


function  split_80_20(preProcessedPath)
 
    mkdir(preProcessedPath, "test");
    mkdir(preProcessedPath, "train");
    
    s = strcat(preProcessedPath, '*.mat');
    files = dir(s);
    n = length(files);
    
    for i=1:n
        sampleName = files(i).name;
        source = strcat(preProcessedPath, sampleName);
        if(mod(i,5)==0)
             destination = strcat(preProcessedPath, "test/", sampleName);
             copyfile(source, destination);
        else
             destination = strcat(preProcessedPath, "train/", sampleName);
             copyfile(source, destination);
        end
    end 
   
end


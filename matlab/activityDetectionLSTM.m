%% =====================================================================================
%% Trains an LSTM using CSI data.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

function trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels)

    % training parameters
    maxEpochs = 100;
    miniBatchSize = 5; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set
    
    % network parameters
    numHiddenUnits = 400;
    numClasses = 7 % standing, walking, running, sitting, laying, falling, no activity
    % classNames = categorical({'standing'; 'walking'; 'standingUp'; 'sitting'; 'sittingDown'; 'empty'; 'falling'});
  
    % set-up the network architecture
    layers = [ ...
        sequenceInputLayer(19) % 1368 684 228 subcarriers x (intensity, phase)
        lstmLayer(numHiddenUnits,'OutputMode','last') 
        dropoutLayer(0.2)
        fullyConnectedLayer(numClasses)
        softmaxLayer('Name', 'finalSM') % to obtain a probability density function
        classificationLayer('Name', 'opinion')]; %, 'Classes', classNames)]; % compare the probability distribution to the one defined by the label
    
    % define the training options
    
    options = trainingOptions('adam', ...
        'ExecutionEnvironment', 'gpu', ...
        'MaxEpochs', maxEpochs, ... % number of runs over the complete data-set
        'MiniBatchSize', miniBatchSize, ... % number of samples between weights update
        'ValidationData', {testSequences, testLabels}, ...
        'ValidationFrequency', 1000, ...
        'Shuffle', 'every-epoch', ...
        'GradientThreshold', 1, ...
        'Verbose', false, ...
        'Plots', 'training-progress'); % show training progress

    % start training
    trainedNet = trainNetwork(trainSequences, trainLabels, layers, options);
    
end
%% =====================================================================================
%% Parses data from an experiment trial (a cell array) and outputs 2 2D matrices. The 'I'
%% matrix contains the signal intensity, while the 'P' one contains the phase. For each
%% of them:
%%
%%   X -> order of arrival of the network packet. Size: varies
%%   Y -> subcarrier index. Subcarriers from the same antenna pair are grouped together. Size: 684
%%
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

function [I, P] = flatten_csi_3x1_averaged(datFile)

    % Subcarriers from the same sender/receiver antenna pair are grouped together
    I = zeros(length(datFile), 57); % allocate the intensity matrix
    P = zeros(length(datFile), 57); % allocate the phase matrix
    goodPackets = 1; % number of good packets the file
    
    for i = 1:length(datFile) % for each packet in the file...
        packet = datFile(i);
        csi = packet{1}.csi; % extract the csi data
        
        dim = size(csi, 2);
        if dim ~= 2 % bad packet
           continue; % skip this packet
        end
        
        if size(csi,3) == 56 % bad packet
           continue; % skip this packet
        end
        
        goodPackets = goodPackets +1;
        for sub = 1:6:size(csi,3) % for each sub-carrier...
          for r = 1:size(csi,1) % for each receiver antenna...
            I(goodPackets, ceil(sub/6)+19*(r-1)) = (abs(csi(2,1,sub))+abs(csi(2,1,sub+1))+abs(csi(2,1,sub+2))+abs(csi(2,1,sub+3))+abs(csi(2,1,sub+4))+abs(csi(2,1,sub+5)))/6; % real(csi(r,s,sub)); % save the intensity value
            P(goodPackets, ceil(sub/6)+19*(r-1)) = angle(csi(2,1,sub)); % imag(csi(r,s,sub)); % save the phase value
          end
        end
    end
    
    % cut the last rows of the matrices because they have not been filled (it's all
    % zeros)
    I(goodPackets:i,:) = [];
    P(goodPackets:i,:) = [];
end
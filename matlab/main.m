%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================
%{
reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'one';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\1\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\1\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
C = plotconfusion(testLabels, predLabels)

%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'two';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\2\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\2\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
C = plotconfusion(testLabels, predLabels)

%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'three';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\3\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\3\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
C = plotconfusion(testLabels, predLabels)

%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'four';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\4\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\4\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
figure;
C = plotconfusion(testLabels, predLabels)

%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'five';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\5\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\5\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
figure;
C = plotconfusion(testLabels, predLabels)
%}
%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

reset(gpuDevice(1)) % clears the GPU memory, to avoid out-of-memory errors

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'six';
% train data path
%trainDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/train/';+
trainDataPath = 'j:\CSI\data\preProcessed\2020\20and22\6\train\';
% test data path
%testDataPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/20_08_19/3x1_6averaged/test/';
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\5\test\'; 

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);
%save("trainDataset", 'trainSequences', 'trainLabels');

% TEST DATA-SET
% select all files that end in '.mat' from the directory
[testSequences, testLabels] = buildDataSet(testDataPath);
%save("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
accuracy = sum(predLabels == testLabels)./numel(testSequences)
figure;
C = plotconfusion(testLabels, predLabels)

%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

% << PARAMETERS >>
miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

load("trainDataset", 'trainSequences', 'trainLabels');
load("testDataset", 'testSequences', 'testLabels');

% train the net
trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels);

% save the trained network to file
fileName = strcat(networkName, '.net');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
acc = sum(predLabels == testLabels)./numel(testSequences)
C = plotconfusion(testLabels, predLabels)
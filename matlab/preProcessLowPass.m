%% =====================================================================================
%% Pre-processes the data obtained by the Atheros CSI Tool and makes it ready to be used
%% for training an LSTM.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

minPackets = 400; 

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\1\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_1of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\2\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_2of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\3\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_3of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\4\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_4of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\5\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_5of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end

% data files
%inPath = '/media/raul/Ext_NVMe/CSI/data/raw/22_08_19/'; % the directory where raw data is located
inPath = 'G:\CSI\data\raw\22_08_19\';
%outPath = '/media/raul/Ext_NVMe/CSI/data/preProcessed/2020/22_08_19/6networks_6averaged/1/'; % the directory where to save the pre-processed data
outPath = 'G:\CSI\data\preProcessed\2020\22_08_19\6networks_6averaged\6\';
mkdir(outPath);

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_6of6(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    DI = single(DI);
    DP = single(DP);
    
    % lowpass filter
    DI = lowpass(DI, 5, 100);
    
    % build the final matrix
    % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    % csi = cat(1, transpose(DI), transpose(DP)); % intensity and phase
    csi = transpose(DI); % intensity only
    % csi = transpose(I);
    % csi = cat(1, transpose(I), transpose(P)); % not denoised, intensity and phase
    % csi = cat(2, transpose(DI), transpose(DP)); % intensity and phase as a 3D matrix
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end
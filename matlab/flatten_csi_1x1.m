%% =====================================================================================
%% Parses data from an experiment trial (a cell array) and outputs 2 2D matrices. The 'I'
%% matrix contains the signal intensity, while the 'P' one contains the phase. For each
%% of them:
%%
%%   X -> order of arrival of the network packet. Size: varies
%%   Y -> subcarrier index. Subcarriers from the same antenna pair are grouped together. Size: 684
%%
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

function [I, P] = flatten_csi_1x1(datFile)

    % Subcarriers from the same sender/receiver antenna pair are grouped together
    I = zeros(length(datFile), 114); % allocate the intensity matrix
    P = zeros(length(datFile), 114); % allocate the phase matrix
    goodPackets = 1; % number of good packets the file
    
    for i = 1:length(datFile) % for each packet in the file...
        packet = datFile(i);
        csi = packet{1}.csi; % extract the csi data
        
        dim = size(csi, 2);
        if dim ~= 2 % bad packet
           continue; % skip this packet
        end
        
        goodPackets = goodPackets +1;
        for sub = 1:size(csi,3) % for each sub-carrier...
            I(goodPackets, sub) = abs(csi(2,1,sub)); % real(csi(r,s,sub)); % save the intensity value
            P(goodPackets, sub) = angle(csi(2,1,sub)); % imag(csi(r,s,sub)); % save the phase value
       end
    end
    
    % cut the last rows of the matrices because they have not been filled (it's all
    % zeros)
    I(goodPackets:i,:) = [];
    P(goodPackets:i,:) = [];
end
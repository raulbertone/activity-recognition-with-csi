%% =====================================================================================
%% Trains an LSTM using CSI data.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

function trainedNet = fallBasicLSTM(trainSequences, trainLabels, testSequences, testLabels)

    % training parameters
    maxEpochs = 10;
    miniBatchSize = 25; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set
    
    % network parameters
    numHiddenUnits = 50;
    numClasses = 2 
    classNames = categorical({'emergency'; 'falseAlarm'});
  
    % set-up the network architecture
    layers = [ ...
        sequenceInputLayer(684) % 1368 684 subcarriers x (intensity, phase)
        lstmLayer(numHiddenUnits,'OutputMode','last') 
        dropoutLayer(0.5)
        fullyConnectedLayer(numClasses)
        softmaxLayer('Name', 'finalSM') % to obtain a probability density function
        classificationLayer('Name', 'opinion', 'Classes', classNames)]; % compare the probability distribution to the one defined by the label
    
    % define the training options
    options = trainingOptions('adam', ...
        'ExecutionEnvironment', 'cpu', ...
        'MaxEpochs', maxEpochs, ... % number of runs over the complete data-set
        'MiniBatchSize', miniBatchSize, ... % number of samples between weights update
        'ValidationData', {testSequences, testLabels}, ...
        'ValidationFrequency', 50, ...
        'Shuffle', 'every-epoch', ...
        'GradientThreshold', 1, ...
        'Verbose', false, ...
        'Plots', 'training-progress'); % show training progress

    % start training
    trainedNet = trainNetwork(trainSequences, trainLabels, layers, options);
    
end
% 5 networks averaged

% load a network
% run it on the test dataset
% save a matrix with 7 columns, the network's opinion about the 7
% categories
% average the corresponding entries of all 5 networks (once with the same
% weights, once weighted according to their accuracy)
% select max
% check accuracy

netOne = load('one.mat');
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\1\test\';
[testSequences, testLabels] = buildDataSet(testDataPath);
[predLabels1, scores1] = classify(netOne.trainedNet, testSequences, 'MiniBatchSize', 25);
accuracy1 = sum(predLabels1 == testLabels)./numel(testSequences);

netTwo = load('two.mat');
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\2\test\';
[testSequences, testLabels] = buildDataSet(testDataPath);
[predLabels2, scores2] = classify(netTwo.trainedNet, testSequences, 'MiniBatchSize', 25);
accuracy2 = sum(predLabels2 == testLabels)./numel(testSequences);

netThree = load('three.mat');
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\3\test\';
[testSequences, testLabels] = buildDataSet(testDataPath);
[predLabels3, scores3] = classify(netThree.trainedNet, testSequences, 'MiniBatchSize', 25);
accuracy3 = sum(predLabels3 == testLabels)./numel(testSequences);

netFour = load('four.mat');
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\4\test\';
[testSequences, testLabels] = buildDataSet(testDataPath);
[predLabels4, scores4] = classify(netFour.trainedNet, testSequences, 'MiniBatchSize', 25);
accuracy4 = sum(predLabels4 == testLabels)./numel(testSequences);

netFive = load('five.mat');
testDataPath = 'j:\CSI\data\preProcessed\2020\20and22\5\test\';
[testSequences, testLabels] = buildDataSet(testDataPath);
[predLabels5, scores5] = classify(netFive.trainedNet, testSequences, 'MiniBatchSize', 25);
accuracy5 = sum(predLabels5 == testLabels)./numel(testSequences);

average = (scores1 + scores2 + scores3 + scores4 + scores5)/5;
[val, index] = max(average, [], 2);
B = categorical(index, [1 2 3 4 5 6 7], {'empty', 'falling', 'sitting', 'sittingDown', 'standing', 'standingUp', 'walking'});
accuracy = sum(B == testLabels)./numel(testSequences);

weightedAverage = ((scores1.^2*accuracy1) + (scores2.^2*accuracy2) + (scores3.^2*accuracy3) + (scores4.^2*accuracy4) + (scores5.^2*accuracy5))/(accuracy1 + accuracy2 + accuracy3 + accuracy4 + accuracy5);
[weightedVal, weightedIndex] = max(weightedAverage, [], 2);
C = categorical(weightedIndex, [1 2 3 4 5 6 7], {'empty', 'falling', 'sitting', 'sittingDown', 'standing', 'standingUp', 'walking'});
weightedAccuracy = sum(C == testLabels)./numel(testSequences);


function plot_csi(M)
    Mt = transpose(M); % to have time on the x-axis    
    surf(Mt)
    colormap(pink) % change color map
    shading interp    % interpolate colors across lines and faces
    xlabel('Packets') 
    ylabel('Subcarriers')
    axis([1 size(M,1) 1 1026 -600 600])
end